package com.kali.tinnolabtest;

import android.databinding.ObservableField;
import android.os.Parcel;
import android.os.Parcelable;

public class Developer implements Parcelable {
    Developer(String name, String mobile, String email, String remark){
        this.name.set(name);
        this.mobile.set(mobile);
        this.email.set(email);
        this.remark.set(remark);
        this.balance.set("");
        this.picture.set("");
    }
    Developer(String name, String mobile, String email, String remark, String balance){
        this.name.set(name);
        this.mobile.set(mobile);
        this.email.set(email);
        this.remark.set(remark);
        this.balance.set(balance);
        this.picture.set("");
    }
    Developer(String name, String mobile, String email, String remark, String balance, String URL){
        this.name.set(name);
        this.mobile.set(mobile);
        this.email.set(email);
        this.remark.set(remark);
        this.balance.set(balance);
        this.picture.set(URL);
    }
    Developer(){
        this("","","","");
    }
    
    public final ObservableField<String> name = new ObservableField<>();
    public final ObservableField<String> mobile = new ObservableField<>();
    public final ObservableField<String> email = new ObservableField<>();
    public final ObservableField<String> remark = new ObservableField<>();
    public final ObservableField<String> balance = new ObservableField<>();
    public final ObservableField<String> picture = new ObservableField<>();
    
    // In constructor you will read the variables from Parcel. Make sure to read them in the same sequence in which you have written them in Parcel.
    Developer(Parcel in) {
        this.name.set(in.readString());
        this.mobile.set(in.readString());
        this.email.set(in.readString());
        this.remark.set(in.readString());
        this.balance.set(in.readString());
        this.picture.set(in.readString());
    }
    
    @Override
    public int describeContents() {
        return Parcelable.CONTENTS_FILE_DESCRIPTOR;
    }
    
    // This is where you will write your member variables in Parcel. Here you can write in any order. It is not necessary to write all members in Parcel.
    @Override
    public void writeToParcel(Parcel dest, int flags) {
// Write data in any order
        dest.writeString(this.name.get());
        dest.writeString(this.mobile.get());
        dest.writeString(this.email.get());
        dest.writeString(this.remark.get());
        dest.writeString(this.balance.get());
        dest.writeString(this.picture.get());
    }
    
    
    // This is to de-serialize the object
    public static final Parcelable.Creator<Developer> CREATOR = new Parcelable.Creator<Developer>(){
        public Developer createFromParcel(Parcel in) {
            return new Developer(in);
        }

        public Developer[] newArray(int size) {
            return new Developer[size];
        }
    };
}
