package com.kali.tinnolabtest;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.kali.tinnolabtest.databinding.ActivityEditBinding;

public class Activity000 extends AppCompatActivity {
    
    Context context = this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        com.kali.tinnolabtest.databinding.Activity000Binding binding = DataBindingUtil.setContentView(this, R.layout.activity_000);
        binding.buttonPart1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goPart1();
            }
        });
        binding.buttonPart2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goPart2();
            }
        });
    }
    
    private void goPart1(){
        Intent intent = new Intent(context, Activity001.class);
        intent.putExtra("part", "1");
        startActivity(intent);
    }
    
    private void goPart2(){
        Intent intent = new Intent(context, Activity001.class);
        intent.putExtra("part", "2");
        startActivity(intent);
    }
}
