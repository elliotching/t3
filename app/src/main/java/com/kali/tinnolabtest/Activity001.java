package com.kali.tinnolabtest;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Activity001 extends AppCompatActivity {
    private AppCompatActivity activity = this;
    private Context context = this;
    private ListView listView;
    private ArrayList<Developer> devList;
    private AdapterDevList adapter;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_001);
        this.getSupportActionBar().setTitle("Developers");
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        String part = activity.getIntent().getStringExtra("part");
        if(part.equals("1")){
            part1();
        }
        else if(part.equals("2")){
            part2();
        }
    }
    
    private void part1(){
        devList = new ArrayList<>();
        for(int i = 0; i<20;i++){
            devList.add(new Developer("Developer #"+i, "Phone Number #"+i, "", ""));
        }
        listView = findViewById(R.id.list_view);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                goEdit(i);
            }
        });
        refreshAndPopulate();
    }
    
    private void refreshAndPopulate() {
        adapter = new AdapterDevList(context, devList);
        listView.setAdapter(adapter);
    }
    
    private void goEdit(int i) {
        Intent intent = new Intent(context, EditActivity.class);
        intent.putExtra("title", devList.get(i).name.get());
        intent.putExtra("developer", (Parcelable) devList.get(i));
        intent.putExtra("part", activity.getIntent().getStringExtra("part"));
        startActivity(intent);
    }
    
    private void part2() {
        devList = new ArrayList<>();
        String data = context.getResources().getString(R.string.json_data);
        try {
            JSONArray jsonArray = new JSONArray(data);
            for(int i = 0; i<jsonArray.length();i++) {
                JSONObject jsonObject = jsonArray.optJSONObject(i);
                String name = jsonObject.optString("name");
                String phone = jsonObject.optString("phone");
                String email = jsonObject.optString("email");
                String balance = jsonObject.optString("balance");
                String isActive = jsonObject.optString("isActive");
                boolean _isActive = jsonObject.optBoolean("isActive");
                String picture = jsonObject.optString("picture");
                String age = jsonObject.optString("age");
                int _age = jsonObject.optInt("age");
                String gender = jsonObject.optString("gender");
                String address = jsonObject.optString("address");
                String registered = jsonObject.optString("registered");
                String index = jsonObject.optString("index");
                int _index = jsonObject.optInt("index");
                String eyeColor = jsonObject.optString("eyeColor");
                
                String remark = "Index: "+index+"\n\n"+
                        "Age: "+age+"\n\n"+
                        "Gender: "+gender+"\n\n"+
                        "Eye Color: "+eyeColor+"\n\n"+
                        "Address: "+address+"\n\n"+
                        "Registered at: "+registered;
                if(_isActive) {
                    devList.add(new Developer(name, phone, email, remark, balance, picture));
                }
            }
            listView = findViewById(R.id.list_view);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    goEdit(i);
                }
            });
            refreshAndPopulate();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            activity.finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
