package com.kali.tinnolabtest;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class AdapterDevList extends BaseAdapter {
    
    private Context context;
    
    private ArrayList<Developer> data;
    
    public AdapterDevList(Context context, ArrayList<Developer> data) {
        this.data = data;
        this.context = context;
    }
    
    @Override
    public int getCount() {
        return data.size();
    }
    
    @Override
    public Object getItem(int position) {
        return data.get(position);
    }
    
    @Override
    public long getItemId(int position) {
        return position;
    }
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ItemHolder holder;
        View view = convertView;
        
        if (view == null) {
            holder = new ItemHolder();
            view = LayoutInflater.from(context).inflate(R.layout.list_item_layout, null/* parent */);
            
            holder.textDevName = view.findViewById(R.id.text_title);
            holder.textDevMobile = view.findViewById(R.id.text_mobile);
            
            view.setTag(holder);
        } else {
            holder = (ItemHolder) view.getTag();
        }

        holder.textDevName.setText(data.get(position).name.get());
        holder.textDevMobile.setText(data.get(position).mobile.get());
        return view;
    }
    
    private class ItemHolder {
        TextView textDevMobile;
        TextView textDevName;
    }
}