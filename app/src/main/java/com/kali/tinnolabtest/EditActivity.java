package com.kali.tinnolabtest;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.kali.tinnolabtest.databinding.ActivityEditBinding;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.Locale;

public class EditActivity extends AppCompatActivity {
    private AppCompatActivity activity = this;
    private Context context = this;
    private ImageView picture;
    private EditText editText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityEditBinding binding = DataBindingUtil.setContentView(activity, R.layout.activity_edit);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.getSupportActionBar().setTitle(this.getIntent().getStringExtra("title"));
        Developer developer = (Developer) this.getIntent().getParcelableExtra("developer");
        binding.setDev(developer);
        binding.buttonRemark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showHideRemark();
            }
        });
        binding.footer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showHideRemark();
            }
        });
        
        if(activity.getIntent().getStringExtra("part").equals("2")) {
            picture = findViewById(R.id.picture);
            Ion.with(context)
                    .load(developer.picture.get())
                    .withBitmap()
                    .placeholder(R.drawable.ic_image)
                    .intoImageView(picture)
                    .setCallback(new FutureCallback<ImageView>() {
                        @Override
                        public void onCompleted(Exception e, ImageView result) {
                            if (result == null) {
                                picture.setImageResource(R.drawable.ic_broken);
                            }
                        }
                    });
    
            Ion.with(context)
                    .load(developer.picture.get())
                    .asString()
                    .setCallback(new FutureCallback<String>() {
                        @Override
                        public void onCompleted(Exception e, String result) {
                            if (result != null) {
                                if (result.contains("html")) {
                                    picture.setImageResource(R.drawable.ic_broken);
                                }
                            } else {
                                picture.setImageResource(R.drawable.ic_broken);
                            }
                        }
                    });
        }
    }
    private void showHideRemark(){
        // status = 1 >> expanding
        // status = -1 >> collapsing
        // status = 0 >> stopped
        editText = findViewById(R.id.edit_remark);
        if(editText.getVisibility() == View.VISIBLE){
            collapse(editText);
        }
        else if(editText.getVisibility() == View.GONE){
            expand(editText);
        }
        
    }
    
    // source: https://stackoverflow.com/questions/4946295/android-expand-collapse-animation
    public void expand(final View v) {
        final int targetHeight = getPixels(context, 500.f);
        
        v.getLayoutParams().height = 1;
        v.requestLayout();
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation()
        {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
//                v.getLayoutParams().height = interpolatedTime == 1 ? LinearLayout.LayoutParams.WRAP_CONTENT : (int)(targetHeight * interpolatedTime);
                v.getLayoutParams().height = interpolatedTime == 1 ? targetHeight : (int)(targetHeight * interpolatedTime);
                if(interpolatedTime == 1){
                    v.getLayoutParams().height = targetHeight;
                }
                else{
                    int k = (int)(targetHeight * interpolatedTime);
                    if(k <= 1){
                        v.getLayoutParams().height = 1;
                    }
                    else{
                        v.getLayoutParams().height = k;
                    }
                }
                v.requestLayout();
                
                Log.d("interpolatedTime", String.valueOf(interpolatedTime));
                Log.d("v.getLayoutParams()", String.valueOf(v.getLayoutParams().height));
            }
            
            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };
        
        // 1dp/ms
        a.setDuration((int)(targetHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }
    
    // source: https://stackoverflow.com/questions/4946295/android-expand-collapse-animation
    public static void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();
        
        Animation a = new Animation()
        {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if(interpolatedTime == 1){
                    v.setVisibility(View.GONE);
                }else{
                    v.getLayoutParams().height = initialHeight - (int)(initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }
            
            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };
        
        // 1dp/ms
        a.setDuration((int)(initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }
    
    // created by elliotching
    private int getPixels(Context context,  float unitDP){
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        float dp = unitDP;
        float fpixels = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, metrics);
        int pixels = Math.round(fpixels);
        return pixels;
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            activity.finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
